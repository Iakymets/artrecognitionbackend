# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.decorators.csrf import csrf_exempt

import cv2
from PIL import Image


from keras.applications.mobilenet import MobileNet, preprocess_input
from keras.preprocessing import image
from keras.models import Model
from keras.models import model_from_json
from keras.layers import Input

# other imports
from sklearn.linear_model import LogisticRegression
import numpy as np
import os
import json
import pickle
import h5py
from keras.models import model_from_json

import cv2

from django.http import JsonResponse
from django.forms.models import model_to_dict

from django.shortcuts import render
from .models import ArtWork, Location


with open('recognition/conf/conf.json') as f:
        config = json.load(f)

locations = list(Location.objects.all().values('id', 'classifier'))
classifiers = {}

print ("[INFO] loading the classifier...")
for location in locations:
    classifiers[str(location['id'])] = pickle.load(open(location['classifier'], 'rb'))

json_file = open('models/model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)

model.load_weights("models/model.h5")
model._make_predict_function()

train_labels = list(ArtWork.objects.all().values_list('id', flat=True))
train_labels.sort()

print(train_labels)

@csrf_exempt
def recognize(request):
    if request.method == 'POST':
        res = { 'prediction': None }
        locationId = request.POST['hash']
        print locationId
        if not locationId:
            return JsonResponse(res)
        img = Image.open(request.FILES['file'])
        # img.save("out.jpg", "JPEG", quality=80, optimize=True, progressive=True)

        x               = image.img_to_array(img)
        x               = np.expand_dims(x, axis=0)
        x               = preprocess_input(x)
        feature         = model.predict(x)
        flat            = feature.flatten()
        flat            = np.expand_dims(flat, axis=0)
        m = classifiers[locationId].predict_proba(flat)
        preds           = classifiers[locationId].predict(flat)
        prediction      = train_labels[preds[0]]

        if m[0][preds[0]] > 0.75:
            artwork = ArtWork.objects.get(id = prediction)
            res['prediction'] = prediction
            res['artwork'] = model_to_dict(artwork)
            print(res)
            # cv2.putText(frame, "I think it is a " + prediction + " (" + str(m[0][preds[0]]) + ")", (40,445), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2);
            print(prediction, str(m[0][preds[0]]))
        return JsonResponse(res)

    return JsonResponse({})

