# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid

from django.db import models

class Location(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    longitude = models.FloatField()
    latitude = models.FloatField()
    radius = models.IntegerField()
    classifier = models.CharField(max_length = 250)

    class Meta:
        db_table = "location"

    def __str__(self):
        return "%s (%f %f)" % (self.id, self.longitude, self.latitude)

class ArtWork(models.Model):
    GENRES = (
        ('landscape', 'Landscape'),
        ('abstraction', 'Abstraction'),
        ('still life', 'Still Life'),
        ('portrait', 'Portrait'),
        ('marine', 'Marine'),
    )
    id = models.CharField(max_length = 50, primary_key=True)
    title = models.CharField(max_length = 250)
    description = models.TextField()
    author = models.CharField(max_length = 250)
    year = models.IntegerField()
    genre = models.CharField(max_length = 50, choices=GENRES)

    location = models.ForeignKey(Location, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = "artwork"

    def __str__(self):
        return self.title

