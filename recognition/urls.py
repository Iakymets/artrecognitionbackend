
from django.conf.urls import url

from . import views

urlpatterns = [
    url('recognize/', views.recognize, name='recognize'),
]
